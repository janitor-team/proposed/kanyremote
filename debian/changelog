kanyremote (8.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Philipp Huebner ]
  * New upstream version 8.0
  * Refreshed patches
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * Updated debhelper compat version: 12
  * Switched from Python 2 to Python 3
  * Added debian/source/lintian-overrides
  * Added debian/upstream/metadata

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 08:54:43 +0200

kanyremote (6.4-2) unstable; urgency=medium

  * Updated Standards-Version: 4.1.3 (no changes needed)
  * Updated Vcs-* fields in debian/control
  * Improved appstream metadata
  * Updated debian/copyright
  * Fixed typo in menu.patch

 -- Philipp Huebner <debalance@debian.org>  Tue, 06 Feb 2018 18:18:39 +0100

kanyremote (6.4-1) unstable; urgency=medium

  * Updated debian/copyright
  * New upstream version 6.4
  * debian/control:
     - Updated Standards-Version: 3.9.6 (no changes needed)
     - Updated Vcs-* fields
     - Switched dependency from pyqt4 to pyqt5 (Closes: #864966)
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Mon, 26 Jun 2017 11:22:55 +0200

kanyremote (6.3.5-1) unstable; urgency=medium

  * Imported Upstream version 6.3.5
  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Adjusted years in debian/copyright
  * debian/rules: let dh handle autotools-dev automatically
  * Added Vcs links to debian/control

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Apr 2015 13:26:19 +0200

kanyremote (6.3.3-1) unstable; urgency=medium

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Thu, 28 Aug 2014 09:38:08 +0200

kanyremote (6.3.2-1) unstable; urgency=low

  * New upstream release
  * Adjusted years in debian/copyright
  * Updated Standards-Version: 3.9.5 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Tue, 04 Feb 2014 20:08:02 +0100

kanyremote (6.3.1-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Wed, 20 Nov 2013 21:05:33 +0100

kanyremote (6.2-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2013 20:26:23 +0200

kanyremote (6.1-1) unstable; urgency=low

  * New upstream release
  * Updated Standards-Version: 3.9.4 (no changes needed)
  * Increased dependency on anyremote to 6.1
  * Switched to debhelper 9 and dh_python2
  * Adjusted years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 12 Mar 2013 00:15:48 +0100

kanyremote (5.13-1) unstable; urgency=low

  * New upstream release
  * Adjusted years in debian/copyright
  * Increased dependency on anyremote to 5.4.1
  * Updated Standards-Version: 3.9.2 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 02 Jan 2012 17:12:29 +1100

kanyremote (5.12-1) unstable; urgency=low

  * New upstream release
  * Adjusted years in debian/copyright
  * Switched to my @debian.org e-mail address in debian/copyright and
    debian/kanyremote.1x

 -- Philipp Huebner <debalance@debian.org>  Fri, 18 Mar 2011 14:00:01 +0100

kanyremote (5.11.9-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Thu, 21 Oct 2010 16:48:14 +0200

kanyremote (5.11.8-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   - Changed Architecture from any to all
   - Removed ${shlibs:Depends}

 -- Philipp Huebner <debalance@debian.org>  Fri, 27 Aug 2010 11:54:06 +0200

kanyremote (5.11.7-1) unstable; urgency=low

  * New upstream release
  * Updated Standards-Version: 3.9.1 (no changes needed)
  * Switched to dpkg-source 3.0 (quilt) format
  * Switched from dpatch to quilt
  * Switched from debhelper 6 to 7
  * Redesigned debian/rules
  * Removed debian/README.Source
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Thu, 29 Jul 2010 14:44:40 +0200

kanyremote (5.11.4-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Sun, 21 Mar 2010 12:12:54 +0100

kanyremote (5.11.3-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Fri, 19 Feb 2010 11:20:33 +0100

kanyremote (5.11.2-1) unstable; urgency=low

  * New upstream release
  * Updated Standards-Version: 3.8.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 01 Feb 2010 22:10:48 +0100

kanyremote (5.11.1-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Wed, 27 Jan 2010 20:30:27 +0000

kanyremote (5.11-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@debian.org>  Fri, 22 Jan 2010 16:00:42 +0000

kanyremote (5.10.1-2) unstable; urgency=low

  * Raised dependency on anyremote on >= 4.18.1

 -- Philipp Huebner <debalance@debian.org>  Sun, 18 Oct 2009 18:27:53 +0200

kanyremote (5.10.1-1) unstable; urgency=low

  * New upstream release
  * Updated maintainer's mail address
  * Updated Standards-Version: 3.8.3 (no changes needed)
  * Removed deprecated call of dh_desktop
  * Added bluez to Recommendis
  * Switched from python-kde3 to python-kde4 and from python-qt3 to python-qt4

 -- Philipp Huebner <debalance@debian.org>  Mon, 24 Aug 2009 08:10:25 +0200

kanyremote (5.8.2-1) unstable; urgency=low

  * New upstream release

 -- Philipp Huebner <debalance@arcor.de>  Sun, 19 Apr 2009 17:27:55 +0200

kanyremote (5.8.1-1) unstable; urgency=low

  * New upstream release
  * debian/rules: replaced 'patch' with '$(DPATCH_STAMPFN)'
  * Updated Standards-Version: 3.8.1 (no changes needed)

 -- Philipp Huebner <debalance@arcor.de>  Fri, 10 Apr 2009 23:28:03 +0200

kanyremote (5.6.1-1) unstable; urgency=low

  * New upstream release
    Fixes "NameError: global name 'cfg' is not defined" (Closes: #516169)

 -- Philipp Huebner <debalance@arcor.de>  Thu, 19 Feb 2009 22:17:32 +0100

kanyremote (5.6-1) unstable; urgency=low

  * New upstream release
  * Removed html-path.dpatch as it's now implemented upstream
  * Removed doc.dpatch
  * Modified debian/rules to remove superfluous docs
  * Adjusted years in debian/copyright
  * Added dh_desktop in debian/rules

 -- Philipp Huebner <debalance@arcor.de>  Thu, 22 Jan 2009 11:40:23 +0100

kanyremote (5.5.1-1) unstable; urgency=low

  * New upstream release
  * Raised dependency on anyremote on >= 4.11
  * Adjusted search path for anyremote's html help (html-path.dpatch
  * Minor changes in debian/copyright

 -- Philipp Huebner <debalance@arcor.de>  Fri, 02 Jan 2009 20:45:38 +0100

kanyremote (5.4-1) unstable; urgency=low

  * New upstream release
  * Corrected watch file
  * re-generated doc.dptach to apply on new upstream release

 -- Philipp Huebner <debalance@arcor.de>  Wed, 22 Oct 2008 08:36:52 +0200

kanyremote (5.2.1-1) unstable; urgency=low

  * New upstream release
  * Added watch file
  * Corrected debian/copyright to point towards GPLv2
  * Corrected some times in /debian docs
  * Removed encoding key in kanyremote.desktop

 -- Philipp Huebner <debalance@arcor.de>  Mon, 15 Sep 2008 13:16:21 +0200

kanyremote (5.0-1) unstable; urgency=low

  * Initial release (Closes: #486546)

 -- Philipp Huebner <debalance@arcor.de>  Mon, 16 Jun 2008 20:23:17 +0200
