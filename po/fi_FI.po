# Finnish translations for kanyremote package.
# Copyright (C) 2009-2012 Mikhail Fedotov
# This file is distributed under the same license as the kanyremote package.
# Mikhail Fedotov <anyremote@mail.ru>, 2009,2010.
#
msgid ""
msgstr ""
"Project-Id-Version: kanyremote 6.0\n"
"Report-Msgid-Bugs-To: anyremote@mail.ru\n"
"POT-Creation-Date: 2019-05-06 23:57+0300\n"
"PO-Revision-Date: 2009-02-15 00:05+0300\n"
"Last-Translator: Matti Jokinen\n"
"Language-Team: Finnish\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: kanyremote:112
msgid "Installed"
msgstr "Asennettu"

#: kanyremote:113
msgid "Not installed"
msgstr "Ei asennettu"

#: kanyremote:115
msgid "No information"
msgstr "Ei tietoja"

#: kanyremote:116
msgid "Available"
msgstr "Saatavilla"

#: kanyremote:117
msgid "Not available"
msgstr "Ei saatavilla"

#: kanyremote:118
msgid "Managed"
msgstr "Hallittu"

#: kanyremote:119
msgid "Running"
msgstr "Käynnissä"

#: kanyremote:121
msgid "Connecting to device"
msgstr "Muodostetaan yhteys laitteeseen"

#: kanyremote:122
msgid "Wait other operations to finish"
msgstr "Ole hyvä ja odota muiden toimintojen päättymistä"

#: kanyremote:124
msgid "TCP port"
msgstr ""

#: kanyremote:125
msgid "Bluetooth channel"
msgstr ""

#: kanyremote:126
msgid "HTTP port"
msgstr ""

#: kanyremote:129 kanyremote:3821
msgid "Downloading"
msgstr "Ladataan"

#: kanyremote:130
#, fuzzy
msgid "Checking J2ME client updates"
msgstr "Haetaan J2ME-klientin päivityksiä"

#: kanyremote:131
#, fuzzy
msgid "No updates for J2ME client were found."
msgstr "J2ME-klientille ei ole päivityksiä saatavilla."

#: kanyremote:1117
msgid "Ping failed !"
msgstr "Ping epäonnistui."

#: kanyremote:1119
msgid "Ping OK !"
msgstr "Ping onnistui."

#: kanyremote:1134 kanyremote:1139 kanyremote:1167
msgid ") to upload !"
msgstr ") ei löydy."

#: kanyremote:1134 kanyremote:1139
#, fuzzy
msgid "Can not find J2ME midlet ("
msgstr "Ladattavaa J2ME-midlet:iä ("

#: kanyremote:1156
msgid ""
"None of bluedevil-sendfile, gnome-obex-send, bluetooth-sendto, blueman-"
"sendto and kbtobexclient are installed !"
msgstr ""
"Mikään seuraavista ei löydy: bluedevil-sendfile, gnome-obex-send, bluetooth-"
"sendto, blueman-sendto, kbtobexclient"

#: kanyremote:1167
msgid "Can not find JAD file ("
msgstr "Ladattavaa JAD-tiedostoa ("

#: kanyremote:1233
msgid "Can not get port to connect to. Is there any device available ?"
msgstr "Porttia, mihin muodostaa yhteys ei löydy."

#: kanyremote:1243
msgid "Connection refused"
msgstr "Yhteydenotto estetty."

#: kanyremote:1264
msgid "AT mode is not supported by phone ("
msgstr "Puhelin ei tue AT-tilaa ("

#: kanyremote:1266
msgid "AT mode could be supported by phone ("
msgstr "Puhelin voi tukea AT-tilaa ("

#: kanyremote:1268
msgid "AT mode is not supported by phone (ERROR response)"
msgstr "Puhelin ei tue AT-tilaa (virhe-vastaus saatu)."

#: kanyremote:1299 kanyremote:1342
msgid "Name"
msgstr "Nimi"

#: kanyremote:1300 kanyremote:1343 kanyremote:1725
msgid "Device Name"
msgstr "Laitteen nimi"

#: kanyremote:1301 kanyremote:1344
msgid "Address"
msgstr "Osoite"

#: kanyremote:1302 kanyremote:1345 kanyremote:1766 kanyremote:1850
#: kanyremote:3013 kanyremote:3180
msgid "Status"
msgstr "Status"

#: kanyremote:1311 kanyremote:1348
msgid "Scan for devices"
msgstr "Etsi laitteita"

#: kanyremote:1312 kanyremote:1349 kanyremote:3054
msgid "Details"
msgstr "Tiedot"

#: kanyremote:1313 kanyremote:1350 kanyremote:2665
msgid "Delete"
msgstr "Poista"

#: kanyremote:1314 kanyremote:1351 kanyremote:1742 kanyremote:1773
#: kanyremote:1854 kanyremote:1974 kanyremote:2164 kanyremote:2205
msgid "Close"
msgstr "Sulje"

#: kanyremote:1318 kanyremote:1352 kanyremote:1777 kanyremote:1855
#: kanyremote:2168 kanyremote:2209 kanyremote:3100 kanyremote:3207
msgid "File"
msgstr "Tiedosto"

#: kanyremote:1341 kanyremote:3090 kanyremote:3205
msgid "Device Browser"
msgstr "Laiteselain"

#: kanyremote:1370
msgid "Scanning"
msgstr "Haetaan"

#: kanyremote:1466
msgid ""
"anyRemote will start only if no other instances of anyRemote are running"
msgstr "anyRemote käynnistyy vain jos muita anyRemote-ilmentymiä ei ole."

#: kanyremote:1517
msgid "Can be useful for Samsung phones"
msgstr "Voi olla hyödyllinen Samsung-puhelimille"

#: kanyremote:1521
msgid "16x16 and 64x64 title icons are available"
msgstr "16x16- ja 64x64-ohjelmaikonit saatavilla"

#: kanyremote:1525
msgid "Can be used on WinMobile devices"
msgstr "Voidaan käyttää WinMobile-laitteilla"

#: kanyremote:1568
#, fuzzy
msgid "It needs to install anyremote-j2me-client package first"
msgstr "anyremote-j2me-client on asennettava ensin"

#: kanyremote:1575 kanyremote:1577
msgid "Bluetooth service is not active"
msgstr "Bluetooth-toiminto ei ole saatavilla"

#: kanyremote:1701
msgid "Wait ping results"
msgstr "Odota pingin tuloksia"

#: kanyremote:1721
msgid "Device Parameters"
msgstr "Laitteen parametrit"

#: kanyremote:1723
#, fuzzy
msgid "BT address"
msgstr "BT-osoite"

#: kanyremote:1726
msgid "Specify Name"
msgstr "Määrittele nimi"

#: kanyremote:1727
msgid " with icon set "
msgstr " ikonien koko "

#: kanyremote:1728
msgid "Run anyRemote when discovered"
msgstr "Käynnistä anyRemote, jos laite havaitaan"

#: kanyremote:1729
msgid "use big title icon"
msgstr "käytä suurta ohjelmaikonia"

#: kanyremote:1730
msgid "use version without JSR-82"
msgstr "käytä versiota ilman JSR-82"

#: kanyremote:1731
msgid "Also upload JAD"
msgstr "Lataa myös JAD"

#: kanyremote:1737
msgid "with all icon sets"
msgstr "sisältäen kaikki ikonit"

#: kanyremote:1738 kanyremote:2666 kanyremote:2674 kanyremote:2719
#: kanyremote:3067 kanyremote:3184
msgid "Choose"
msgstr "Valitse"

#: kanyremote:1739
msgid "Ping"
msgstr "Ping"

#: kanyremote:1740
#, fuzzy
msgid "Upload J2ME"
msgstr "Lataa J2ME"

#: kanyremote:1741
msgid "Test AT"
msgstr "Testaa AT"

#: kanyremote:1743 kanyremote:1975 kanyremote:2671
msgid "OK"
msgstr "OK"

#: kanyremote:1765 kanyremote:1849
msgid "Package"
msgstr "Paketti"

#: kanyremote:1815
msgid "Downloaded"
msgstr "Ladattu"

#: kanyremote:1840
msgid "Warning:"
msgstr "Varoitus:"

#: kanyremote:1841
#, fuzzy
msgid ""
"Installation directory of anyremote-j2me-client not specified in current "
"setup configuration !"
msgstr ""
"anyremote-j2me-client asennushakemistoa ei ole määritelty nykyisessä "
"asetuskonfiguraatiossa."

#: kanyremote:1848
msgid "Configuration Check"
msgstr "Näytä konfiguraatio"

#: kanyremote:1887 kanyremote:1969 kanyremote:3012 kanyremote:3179
msgid "Application"
msgstr "Ohjelma"

#: kanyremote:1888 kanyremote:1970 kanyremote:3014 kanyremote:3181
msgid "Mode"
msgstr "Tila"

#: kanyremote:1889 kanyremote:3016
msgid "F"
msgstr "F"

#: kanyremote:1968
msgid "Choose application"
msgstr "Valitse ohjelma"

#: kanyremote:2162 kanyremote:2206 kanyremote:3087 kanyremote:3202
msgid "Save"
msgstr "Tallenna"

#: kanyremote:2163 kanyremote:2208
msgid "Save As"
msgstr "Tallenna nimellä"

#: kanyremote:2204 kanyremote:3198
msgid "Edit configuration file"
msgstr "Muokkaa konfiguraatiotiedostoa"

#: kanyremote:2207
msgid "Ctrl+S"
msgstr "Ctrl+S"

#: kanyremote:2223
msgid "Can not save the file !"
msgstr "Tiedoston tallentaminen epäonnistui."

#: kanyremote:2265
msgid "General"
msgstr "Yleiset"

#: kanyremote:2277 kanyremote:2672
msgid "Directories"
msgstr "Hakemistot"

#: kanyremote:2280
msgid "Choose directories with configuration files and add them to the list"
msgstr ""
"Valitse hakemistot, joissa konfiguraatiotiedostot sijaitsevat ja lisää ne "
"luetteloon"

#: kanyremote:2343
msgid "Empty field means no update"
msgstr "Jätä tyhjäksi, mikäli et halua päivityksiä"

#: kanyremote:2351
#, fuzzy
msgid ""
"Empty field means no update. Beware: Android/J2ME clients could fail to "
"connect to anyRemote when browsing is in process"
msgstr ""
"Jätä kenttä tyhjäksi, mikäli et halua päivityksiä. Huomaa että Android/J2ME-"
"klientin yhteydenotto anyRemoteen voi epäonnistua haun ollessa käynnissä"

#: kanyremote:2361
#, fuzzy
msgid "Server Mode"
msgstr "Palvelintila"

#: kanyremote:2410
msgid "Files will be saved to $HOME/.anyRemote"
msgstr "Tiedostot tallennetaan hakemistoon $HOME/.anyRemote"

#: kanyremote:2421
msgid "AT mode"
msgstr "AT-tila"

#: kanyremote:2442
msgid "Bemused"
msgstr "Bemused"

#: kanyremote:2459 kanyremote:2647
msgid "iViewer"
msgstr "iViewer"

#: kanyremote:2637 kanyremote:3088 kanyremote:3203
msgid "Preferences"
msgstr "Asetukset"

#: kanyremote:2639
msgid "Show in list : "
msgstr "Näytä seuraavat luettelossa:"

#: kanyremote:2640
#, fuzzy
msgid "Upload J2ME client from "
msgstr "Lataa J2ME-klientti hakemistosta "

#: kanyremote:2641
msgid "Examples"
msgstr "Esimerkit"

#: kanyremote:2642
msgid "Applications"
msgstr "Ohjelmat"

#: kanyremote:2643
msgid "Custom Made"
msgstr "Omat"

#: kanyremote:2644
msgid "Server-mode"
msgstr "Palvelintila"

#: kanyremote:2645
#, fuzzy
msgid "Bemused emulation"
msgstr "Bemused-emulaatio"

#: kanyremote:2646
msgid "AT-mode"
msgstr "AT-tila"

#: kanyremote:2648
#, fuzzy
msgid "Non-available"
msgstr "Ei saatavilla"

#: kanyremote:2649
msgid "Update application list every"
msgstr "Päivitä ohjelmaluettelo"

#: kanyremote:2650 kanyremote:2651
msgid "sec."
msgstr "sekunnin välein."

#: kanyremote:2653
msgid "Use Bluetooth connection, channel"
msgstr ""

#: kanyremote:2654
msgid "Use TCP/IP connection, port"
msgstr ""

#: kanyremote:2655
msgid "Use Web Interace, port"
msgstr ""

#: kanyremote:2656
msgid "Advertise service through Avahi"
msgstr ""

#: kanyremote:2657
msgid "Specify options manually"
msgstr ""

#: kanyremote:2659 kanyremote:2660 kanyremote:2661
msgid "Use connect string"
msgstr "Käytettävä yhdistämismerkkijono"

#: kanyremote:2663
msgid "Run device browser with timeout"
msgstr "Laitehaun aikaraja"

#: kanyremote:2664 kanyremote:2693
msgid "Add"
msgstr "Lisää"

#: kanyremote:2667
#, fuzzy
msgid "Auto reconnect"
msgstr "Muodosta yhteys automaattisesti"

#: kanyremote:2668
msgid "Gnome session"
msgstr "Gnome-istunnon kanssa"

#: kanyremote:2669
msgid "Auto startup with"
msgstr "Käynnistä automaattisesti "

#: kanyremote:2670
msgid "KDE session"
msgstr "KDE-istunnon kanssa"

#: kanyremote:2673
#, fuzzy
msgid "Run on startup"
msgstr "Käynnistä automaattisesti "

#: kanyremote:2675
msgid "Cancel"
msgstr "Peruuta"

#: kanyremote:2676
msgid "Download J2ME client from Web"
msgstr "Lataa Java-klientti Internetistä"

#: kanyremote:2677
msgid "Check J2ME client updates"
msgstr "Hae J2ME-klientin päivityksiä"

#: kanyremote:2678
msgid "Check J2ME client updates at start"
msgstr "Hae J2ME-klientin päivityksiä käynnistettäessä"

#: kanyremote:2819
msgid ""
"There is no item in the list !\n"
"kAnyRemote will not be able to manage any software !"
msgstr "Luettelo on tyhjä, kAnyRemote ei voi hallita ohjelmia."

#: kanyremote:3004 kanyremote:3082 kanyremote:3189 kanyremote:3200
#: kanyremote:4458
msgid "Stop"
msgstr "Pysäytä"

#: kanyremote:3005 kanyremote:3081 kanyremote:3190 kanyremote:3199
#: kanyremote:4457
msgid "Start"
msgstr "Käynnistä"

#: kanyremote:3015 kanyremote:3182
msgid "Type"
msgstr "Tyyppi"

#: kanyremote:3073 kanyremote:3186
msgid "Execute Command"
msgstr "Suorita komento"

#: kanyremote:3079 kanyremote:3191 kanyremote:4460
msgid "Quit"
msgstr "Sulje"

#: kanyremote:3080 kanyremote:3197
msgid "Edit"
msgstr "Muokkaa"

#: kanyremote:3083
msgid "Update Status"
msgstr "Päivitä status"

#: kanyremote:3084 kanyremote:3188
msgid "Close Window"
msgstr "Sulje ikkuna"

#: kanyremote:3089 kanyremote:3204
msgid "Check Configuration"
msgstr "Näytä konfiguraatio"

#: kanyremote:3093 kanyremote:3115 kanyremote:3193 kanyremote:3209
msgid "Help"
msgstr "Ohje"

#: kanyremote:3094 kanyremote:3195 kanyremote:4456
msgid "About"
msgstr "Tietoja ohjelmasta"

#: kanyremote:3109 kanyremote:3208
msgid "Setup"
msgstr "Asetukset"

#: kanyremote:3201
msgid "Stop anyRemote"
msgstr "Pysäytä anyRemote"

#: kanyremote:3214
msgid ""
"This is the first time kAnyRemote runs.\n"
"Please specify directories with anyRemote configuration files."
msgstr ""
"kAnyRemote käynnistetään ensimmäistä kertaa.\n"
"Ole hyvä ja määrittele hakemistot, joissa anyRemoten konfiguraatiotiedostot "
"sijaitsevat."

#: kanyremote:3218
msgid "Would You like to download J2ME client from Web ?"
msgstr "Haluatko ladata J2ME-klientin Internetistä?"

#: kanyremote:3323
msgid "Can not find browser to show help !"
msgstr "Selainta ei löydy. Ohjetta ei voida näyttää."

#: kanyremote:3339
msgid "Can not find documentation !"
msgstr "Dokumentaatiota ei löydy."

#: kanyremote:3486
msgid "anyRemote stopped"
msgstr "anyRemote pysäytetty"

#: kanyremote:3496
#, fuzzy
msgid "Ready to connect on"
msgstr "Muodosta yhteys automaattisesti"

#: kanyremote:3506
msgid "Connected to phone"
msgstr "Yhteys puhelimeen on muodostettu"

#: kanyremote:3508
msgid "Connected"
msgstr "Yhdistetty"

#: kanyremote:3660
msgid ""
"New version of J2ME client is available. Would You like to download it ?"
msgstr "Uusi versio J2ME-klientistä on saatavilla. Haluatko ladata sen?"

#: kanyremote:3785
msgid "Can not establish the connection !"
msgstr "Yhteyttä ei voida muodostaa."

#: kanyremote:3800
msgid "Download failed !"
msgstr "Tiedoston lataaminen epäonnistui !"

#: kanyremote:3809
msgid "Can not download "
msgstr "Lataaminen ei onnistu"

#: kanyremote:3899
msgid "Can not start anyRemote. No configuation file specified!"
msgstr ""

#: kanyremote:4178
msgid "Active"
msgstr "Aktiivinen"

#: kanyremote:4188
msgid ""
"Bluetooth connection will not work. It needs to run bluetoothd daemon with -"
"C option"
msgstr ""

#: kanyremote:4190
msgid "Not active"
msgstr "Ei aktiivinen"

#: kanyremote:4197
msgid ""
"anyRemote not found !\n"
"Please install it or correct $PATH"
msgstr ""
"anyRemote ei löydy !\n"
"Ole hyvä ja asenna anyRemote tai korjaa $PATH"

#: kanyremote:4201
msgid ""
"sdptool not found !\n"
"Please install bluez-util"
msgstr ""
"sdptool ei löydy !\n"
"Ole hyvä ja asenna bluez-util"

#: kanyremote:4392
msgid ""
"Install PyBluez first !\n"
"Or run with --npybluez option"
msgstr ""
"Asenna PyBluez ensin !\n"
"Tai käynnistä parametrilla --npybluez"

#: kanyremote:4459
msgid "Minimize"
msgstr "Pienennä"

#: kanyremote:4459
msgid "Restore"
msgstr "Palauta"

#, fuzzy
#~ msgid "Can not find anyremote-j2me-client installation !"
#~ msgstr "anyRemote-J2ME-klientin asennus ei löydy."

#~ msgid "Disconnected from phone"
#~ msgstr "Yhteys puhelimeen on katkaistu"

#~ msgid ""
#~ "You need to specify the same address and port 5003 in CF iViewer setup on "
#~ "iPhone/iPod Touch"
#~ msgstr "CF iViewer tarvitsee saman osoiteen ja portin 5003iPhone/iPod Touch"

#~ msgid "Use host ip"
#~ msgstr "Palvelimen IP-osoite"

#, fuzzy
#~ msgid "Download iViewer GUI files"
#~ msgstr "Lataa iViewer GUI tiedostot"

#~ msgid "Would You like to change upload java client path ?"
#~ msgstr "Haluatko muokata Java-klientin polkua?"

#~ msgid "Run web interface with parameters"
#~ msgstr "Käynnistä web-rajapinta parametrein"

#~ msgid "anyremote2html is not installed !"
#~ msgstr "anyRemote2html ei ole asennettu."

#~ msgid "Directory selection"
#~ msgstr "Hakemiston valinta"

#~ msgid "All"
#~ msgstr "Kaikki"

#~ msgid "Custom"
#~ msgstr "Omat"

#~ msgid "Example"
#~ msgstr "Esimerkki"

#~ msgid "Install PyGTK first !!!"
#~ msgstr "Ole hyvä ja asenna PyGTK ensin."

#~ msgid "New device"
#~ msgstr "Uusi laite"

#~ msgid "Queue ping request"
#~ msgstr "Aseta ping-pyyntö jonoon"

#~ msgid "Queue push request"
#~ msgstr "Aseta push-pyyntö jonoon"

#~ msgid "Can not read the file "
#~ msgstr "Tiedostoa ei voida lukea"

#~ msgid "Save File As"
#~ msgstr "Tallenna nimellä"

#~ msgid "Properties"
#~ msgstr "Ominaisuudet"

#~ msgid "File selection"
#~ msgstr "Tiedoston valinta"

#~ msgid "Upload"
#~ msgstr "Lataa"

#~ msgid "Override \"Device=\" parameter with"
#~ msgstr "Korvaa \"Device=\"-parametri seuraavalla"

#~ msgid "Name       "
#~ msgstr "Nimi       "

#~ msgid "Address          "
#~ msgstr "Osoite          "

#~ msgid "Status       "
#~ msgstr "Status       "

#~ msgid "Application    "
#~ msgstr "Ohjelma    "

#~ msgid "Status     "
#~ msgstr "Status     "

#~ msgid "Mode    "
#~ msgstr "Tila    "

#~ msgid "Exit"
#~ msgstr "Sulje"

#~ msgid "Device Name:"
#~ msgstr "Laitteen nimi:"

#~ msgid "Check configuration"
#~ msgstr "Näytä konfiguraatio"

#~ msgid "  Device name: "
#~ msgstr " Laitteen nimi: "

#~ msgid " with parameters "
#~ msgstr " parametrein"
