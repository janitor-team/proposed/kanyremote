* Tue Apr 30 2019 Mikhail Fedotov <anyremote at mail.ru> - 8.0
- Port to python3

* Sun Jan 10 2018 Mikhail Fedotov <anyremote at mail.ru> - 7.0
- Some fixes

* Fri Jul 20 2017 Mikhail Fedotov <anyremote at mail.ru> - 6.4
- Move to QT5

* Fri Jan 16 2015 Mikhail Fedotov <anyremote at mail.ru> - 6.3.5
- Avahi support

* Sun Oct 5 2014 Mikhail Fedotov <anyremote at mail.ru> - 6.3.4
- Fix some issues with QIcon class

* Fri Jul 18 2014 Mikhail Fedotov <anyremote at mail.ru> - 6.3.3
- Large application icon and AppData support.

* Mon Dec 2 2013 Mikhail Fedotov <anyremote at mail.ru> - 6.3.2
- Fixed RedHat bugzilla bug 1034914

* Mon Sep 16 2013 Mikhail Fedotov <anyremote at mail.ru> - 6.3.1
- Greek translation was added (Thanks to Ioannis Servetas)

* Fri Aug 16 2013 Mikhail Fedotov <anyremote at mail.ru> - 6.3
- Drop PyKDE dependency, small corrections, fixed RedHat bugzilla bug 988080.

* Mon Jun 10 2013 Mikhail Fedotov <anyremote at mail.ru> - 6.2
- Multiconnection and autostart support.

* Wed Oct 10 2012 Mikhail Fedotov <anyremote at mail.ru> - 6.1
- Drop lightthpd dependency. Translation updates

* Mon Aug 13 2012 Mikhail Fedotov <anyremote at mail.ru> - 6.0.1
- Translation update 

* Fri May 25 2012 Mikhail Fedotov <anyremote at mail.ru> - 6.0
- Update to work with anyremote v6.0, drop support of anyremote2html

* Sun Jan 10 2018 Mikhail Fedotov <anyremote at mail.ru> - 7.0
- Port ganyremote from PyGTK to PyGObject

* Sun Dec 4 2011 Mikhail Fedotov <anyremote at mail.ru> - 5.13
- Add --tray commandline option

* Fri Mar 11 2011 Mikhail Fedotov <anyremote at mail.ru> - 5.12
- Czech translation updated. Correctly works with anyRemote v5.4

* Fri Sep 17 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.9
- Slovak translation updated 

* Wed Aug 4 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.8
- Do not use /sbin/ip if it absent

* Fri Jul 16 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.7
- Docs search path corrected.

* Tue Jul 6 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.6
- Small correction.

* Fri Jul 2 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.5
- Small correction.

* Tue Mar 9 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.4
- Some correction in translations.

* Mon Feb 15 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.3
- Some correction in translations. 128x128 java client icons handling.

* Mon Feb 01 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.2
- Fixed RedHat bugzilla bug 560302

* Wed Jan 27 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.1
- Small updates.

* Fri Jan 22 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11
- Command Fusion iViewer support.

* Mon Jul 6 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.10.2
- Translations were updated.

* Thu Jul 2 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.10
- Tool was rewritten on QT4. Enhanced handling of GuiAppBinary tag.
  Handle java client with 48x48 icons.

* Tue May 26 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.9
- Slovak translation was added (thanks to Michal Toth)

* Fri Apr 9 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.8.2
- Small enhancements 

* Tue Apr 7 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.8.1
- Fix small bug 

* Mon Mar 30 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.8
- Add GuiAppModes tag handling

* Wed Mar 11 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.7
- Finnish and Swedish translation were added (thanks to Matti Jokinen)

* Wed Jan 21 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.6.1-1
- Minor bugfix

* Mon Jan 19 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.6-1
- Check java client version on the web site

* Sun Dec 21 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.5.1-1
- Fix upload from web feature

* Sun Dec 14 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.5-1
- Handle GuiAppVersion parameter in configuration files. Add possibility
  to download java client from Web. Small Ubuntu-specific fixes.

* Wed Dec 3 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.4.1-1
- Fix detection of activity of bluetooth service

* Fri Oct 17 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.4-1
- Enhanced edit configuration file window. Support application details 
  auto wrap. Added Bulgarian translation (thanks to Stanislav Popov)

* Wed Sep 24 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.3-1
- Add icons to menu and buttons.

* Mon Sep 8 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.2.1-1
- Small bugfixes.

* Thu Sep 4 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.2-1
- Added "Details" field to the main window.
  Added French translation (thanks to Philippe Hensel).

* Tue Aug 19 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.1-1
- Fixed crash on startup issue.
  Added Czech (thanks to Tomas Kaluza) and Dutch (thanks to Geert Vanhaute)
  translations.

* Mon Jul 21 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.0-1
- Internationalization support.
  Added Polish (thanks to Marek Szuba), Austrian (thanks to Christian 
  Zehetnertran), German (thanks to Johann Bauer), Spanish (thanks to Carlos 
  S�nchez Mateo and Francisco), Brazilian Portuguese (thanks to Marcos 
  Venilton Batista),Italian (thanks to Massimo Robbiati) Hungarian (thanks to 
  Gyuris Szabolcs) and Russian translations.

* Sun May 25 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.9-1
- Bugfixes and enhancements to better support anyremote-J2ME client v4.6 and
  anyremote2html v0.5.

* Sun Apr 20 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.8-1
- Some small enhancements. Spec file correction.

* Tue Mar 11 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.7-1.fc8
- Some small enhancements. Corrected to work properly with anyRemote v4.4.

* Tue Feb 26 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.6-2.fc8
- Spec file correction

* Wed Feb 20 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.6-1.fc8
- Handle absense of .anyRemote directory

* Sun Feb 17 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.5-2.fc8
- Spec file correction

* Fri Feb 15 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.5-1.fc8
- Motorola RIZR Z3 support enhanced

4.4
---------------------------------------------------------------------------------
Small correction to work well with anyRemote v4.2.

4.3
---------------------------------------------------------------------------------
Small correction to work well with anyRemote v4.1.

Difference beetween 4.2 and 4.1
---------------------------------------------------------------------------------
Small enhancements.

Difference beetween 4.1 and 4.0
---------------------------------------------------------------------------------
Device browser part was added.

Difference beetween 4.0 and 3.3
---------------------------------------------------------------------------------
Actually v4.0 is absolutely new application, not a new version of v3.XX
Since v4.0 kAnyRemote is a GUI front end for anyRemote, written on PyQt/PyKDE. It 
is not a standalone application anymore.

Difference beetween 3.3 and 3.2
---------------------------------------------------------------------------------
Small enhancements. Command Set(disconnect) was introduced. Fixed build issue in Gentoo.

Difference beetween 3.2 and 3.1
---------------------------------------------------------------------------------
Cfg.file for xine (new version) and MythTv (thanks to Tim Hicks) were added.
cfg-examples directory was renamed to cfg-data. *.spec file was changed: solid rpm was
separated into 3 RPM's. Lot of bugfixes and small enhancemens.

Difference beetween 3.1 and 3.0
---------------------------------------------------------------------------------
New command Get(screen_size|model) was introduced. Set(image...) command was enhanced.
In AT-mode nothing will be sent to phone after RemoteOff command.
Java client was corrected towork properly with Nokia-E70.
Cfg.file for Mplayer, Quodlibet, VLC and Nokia-E70 were added. 
A lot of bugfixes.

Difference beetween 3.0 and 2.12
---------------------------------------------------------------------------------
New command Set(image,...) was introduced. It allows to transfer to phone images and then show
them on the screen. According to this feature communication protocol was slightly changed.
Communication code in java client was reworked completely.
Cfg.file for Kpdf, Listen and Open Office Impress were added.
Since this versions all temporary and log files will be created in $HOME/.anyRemote
instead of /tmp.

Difference beetween 2.12 and 2.11
---------------------------------------------------------------------------------
Cfg.file for Aqualung was added. anyRemote was tested over Wi-Fi connection between Nokia-E61 and PC.
Java client starts to support CDLC-1.0 phones. A lot of bugs were fixed.

Difference beetween 2.11 and 2.10
---------------------------------------------------------------------------------
Java client was tested on Motorola-V3i and Nokia-E61. 
Set(iconlist,...) command was introduced. Added possibility to use 16x16 or 32x32 png icons. 
Cfg.file for Kopete was added. A lot of bugfixes and enhancements.

Difference beetween 2.10 and 2.9
---------------------------------------------------------------------------------
Java client was tested on Motorola-L6 and Siemens-S65. New capability to work as 
back-end with separated GUI front-endswas introduced. New commands Set(parameter,...) 
and Set(repaint) was introduced. Command Set(debug,...) was changed to 
Set(parameter,debug,...). A lot of bugs were fixed.
Cfg.files for Exaile was added.

Difference beetween 2.9 and 2.8
---------------------------------------------------------------------------------
New commands Include(...) and ExecAndSet(upload,...) were introduced. Now it is possible 
to upload icons to Java Client. Cfg.files for Audacious, Banshee, BMP, Freevo, JuK, MPD, Totem, 
custom made RSS reader and cfg.file for icon uploading were added.

Difference beetween 2.8 and 2.7
---------------------------------------------------------------------------------
New commands Load(...) and Set(list,select,...) were introduced.
Cfg.files for KdeTv, Gwenview, Rhythmbox, XdTV were added.

Difference beetween 2.7 and 2.6
---------------------------------------------------------------------------------
New command Macro() was introduced. Command Set(list,menu...) was changed to 
Set(menu...). It is possible to add custom menu to any screen of Java Client. 
It is possible to select several files in file manager screen of Java Client.
Several little enhancements and bugfixes.
v2.7.1 differs from v2.7 only by updated Java Client 

Difference beetween 2.6 and 2.5
---------------------------------------------------------------------------------
Several enhancements and serious bugfixes. Set(volume,X) command was introduced to show volume bar.

Difference beetween 2.5 and 2.4
---------------------------------------------------------------------------------
File manager which looks like MC was added to Java Client.
Several enhancements and bugfixes. Xine management example was added.

Difference beetween 2.4 and 2.3
---------------------------------------------------------------------------------
Maintenance release. Java Client was tested with Nokia-6021

Difference beetween 2.3 and 2.2
---------------------------------------------------------------------------------
Enhancemens were made mainly in Java Client. Now it supports: different icon layouts 
("skins"),it can be configured to run in fullscreen mode. 
Exception handling improved and lots of bugs were fixed. 

Difference beetween 2.2 and 2.1
---------------------------------------------------------------------------------
Look and feel of GUI of Java Client was redesigned. Now it is possible to change colors and font size.
Choose actions possiblenot only by numeric key but also by joystick.
Support of lists was added  (now kAnyRemote can be configured to support playlists)


Difference beetween 2.1 and 2.0
---------------------------------------------------------------------------------
Bugfixes and small enhancements. (EnterMode) and (ExitMode) event handlers were added.
Set(text,...) command was added.

Difference beetween 2.0 and 1.4
---------------------------------------------------------------------------------
This release is the first step to create application which could provide
"bemused-like" functionality.
Format of configurational file was changed completely.
Now anyRemote could works as server (like bemused).
Java client was created for JSR82-compatible phones.

Difference beetween 1.4 and 1.3
---------------------------------------------------------------------------------
Concept of key "alias" was introduced.
Tool was tested on Siemens-S55. A lot of bugfixes.

Difference beetween 1.3 and 1.2
---------------------------------------------------------------------------------
Concepts of "mode" was introduced to make possible to set more than one command to the
key. Tool was tested on SE-K750 (BT&IR)
Some bug was fixed and some code was cleaned up.
digikam and kmplayer, kuickshow and web-stream and "mega-example" all-in-one examples were addded.

Difference beetween 1.2 and 1.1
---------------------------------------------------------------------------------
It is possible substuitute current time into command handlers.
Two new handlers for connect and disconnect event were added.
"Greeting" configuration option was deleted in favour of new connect handler.
Anyremote was tested with Motorola-V500 with cable connection.
kview, tvtime and keyboard emulaton examples was added.

Difference beetween 1.1 and 1.0
---------------------------------------------------------------------------------
SonyEricsson-K700-related patches included. Added 2 examples of conf.files (For Sagem and for SE).
Kplayer, KsCd, Noatun, Twinkle examples were added.

Difference beetween 1.0 and 0.9
---------------------------------------------------------------------------------
Sagem-related patches included. Now AnyRemote accept empty +CKEV input also (+CKEV: ,1).

Difference beetween 0.9 and 0.8
---------------------------------------------------------------------------------
Auto-detect phone model. CmerOn/CmerOff conf. parameters are not mandatory now.

Difference beetween 0.8 and 0.7
---------------------------------------------------------------------------------
Auto-muting feature during incoming call.
Switch remote control functionality on/off without disconnect.


Difference beetween 0.7 and 0.5
---------------------------------------------------------------------------------
Handling incoming calls.
It is possible to set multikey command.
-h command line was option added.


0.5
---------------------------------------------------------------------------------
This is first KDE-oriented release. By functionality it equals to console-oriented anyremote-0.4
